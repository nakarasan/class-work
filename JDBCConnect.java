package databaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnect {

	public static void main(String[] args) {

		System.out.println("-----MYSQL JDBC Connection Test");
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("Attached the JDBC Driver");
		} catch (ClassNotFoundException e1) {
			System.err.println("Failed to Attached the JDBC Driver");
			e1.printStackTrace();
		}

		Connection connection = null;

		try {
			connection = DriverManager.getConnection("JDBC:mysql://localhost:3306/csd16", "root", "root");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (connection != null) {
			System.out.println("Successfull !!! Database was Connected");
		} else {
			System.out.println("Failed to make Connection");
		}

	}

}
